//Headers
#include<Windows.h>
#define MYTIMER 101

//global function declrations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	MessageBox(NULL, TEXT("Application is Started"), TEXT("MESSAGE"), MB_OK | MB_ICONINFORMATION);

	//code
	//initialization of WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;//callback funcion name is assigned here
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above  class
	RegisterClassEx(&wndClass);

	//create Window
	hwnd = CreateWindow(szAppName,
		TEXT("MY_APPLICATION"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);


	//message Loop


	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	HBRUSH hbrush;
	TCHAR str[25];
	TCHAR str1[25];
	static int iPaintFlag = -1;
	RECT rcTemp;
	float temp = 0.0f;
	//code
	switch (iMsg)
	{
	case WM_CREATE:
		wsprintf(str, TEXT("WM_CREATE IS ARRIVED"));
		MessageBox(hwnd, str1, TEXT("MESSAGE"), MB_OK | MB_ICONINFORMATION);
		SetTimer(hwnd, MYTIMER, 2000, NULL);
		break;

	case WM_TIMER:
		KillTimer(hwnd,MYTIMER);
		iPaintFlag++;
		if (iPaintFlag > 7)
		{
			iPaintFlag = 0;
		}
		
		if (iPaintFlag == 1)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije

		}
		else if (iPaintFlag == 2)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
		}
		else if (iPaintFlag == 3)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
		}
		else if (iPaintFlag == 4)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
		}
		else if (iPaintFlag == 5)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
		}
		else if (iPaintFlag == 6)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
		}
		else if (iPaintFlag == 7)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
		}
		else if (iPaintFlag == 0)
		{
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
		}
		SetTimer(hwnd, MYTIMER, 2000, NULL);
		break;

	case VK_ESCAPE:
		//DestroyWindow(hwnd);

		break;

	case WM_CHAR:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);

		break;

		/*case 'R':
		case 'r':
			iPaintFlag = 1;
			InvalidateRect(hwnd, NULL, TRUE);//WM_PAINT ADVALA InvalidateRect ni or key dabli asta WM_PAINT ala paije
			break;

		case 'G':
		case 'g':
			iPaintFlag = 2;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		case 'B':
		case 'b':
			iPaintFlag = 3;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		case 'C':
		case 'c':
			iPaintFlag = 4;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		case 'M':
		case 'm':
			iPaintFlag = 5;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		case 'Y':
		case 'y':
			iPaintFlag = 6;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		case 'K':
		case 'k':
			iPaintFlag = 0;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		case 'W':
		case 'w':
			iPaintFlag = 7;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		default:
			iPaintFlag = 0;
		*/
		}
		
		break;
		
	case WM_PAINT:
		GetClientRect(hwnd, &rc);//rc chi width 8 madhe devide keli
		hdc = BeginPaint(hwnd, &ps);
		
		switch (iPaintFlag)
		{
		case 1:
			rcTemp.left = rc.left;//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 8);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		case 2:
			rcTemp.left = rc.left;//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 8);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			
		
			rcTemp.left = rc.left+(rc.right/8);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right/4);
			rcTemp.bottom = rc.bottom;
			temp = rcTemp.left;
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		case 3:
			rcTemp.left = rc.left;//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 8);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);


			rcTemp.left = rc.left + (rc.right / 8);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 4);
			rcTemp.bottom = rc.bottom;
			temp = rcTemp.left;
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left+ (rc.right / 4);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right /2.6);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(0, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		case 4:
			rcTemp.left = rc.left;//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 8);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);


			rcTemp.left = rc.left + (rc.right / 8);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 4);
			rcTemp.bottom = rc.bottom;
			temp = rcTemp.left;
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 4);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 2.6);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(0, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 2.6);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.9);
			rcTemp.bottom = rc.bottom;


			hbrush = CreateSolidBrush(RGB(0, 255, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		case 5:
			rcTemp.left = rc.left;//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 8);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);


			rcTemp.left = rc.left + (rc.right / 8);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 4);
			rcTemp.bottom = rc.bottom;
			temp = rcTemp.left;
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 4);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 2.6);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(0, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 2.6);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.9);
			rcTemp.bottom = rc.bottom;


			hbrush = CreateSolidBrush(RGB(0, 255, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 1.9);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.5);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		case 6:
			rcTemp.left = rc.left;//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 8);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);


			rcTemp.left = rc.left + (rc.right / 8);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 4);
			rcTemp.bottom = rc.bottom;
			temp = rcTemp.left;
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 4);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 2.6);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(0, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 2.6);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.9);
			rcTemp.bottom = rc.bottom;


			hbrush = CreateSolidBrush(RGB(0, 255, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 1.9);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.5);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 1.5);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.3);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		case 7:
			rcTemp.left = rc.left;//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 8);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);


			rcTemp.left = rc.left + (rc.right / 8);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 4);
			rcTemp.bottom = rc.bottom;
			temp = rcTemp.left;
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 4);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 2.6);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(0, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 2.6);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.9);
			rcTemp.bottom = rc.bottom;


			hbrush = CreateSolidBrush(RGB(0, 255, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 1.9);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.5);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 0, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 1.5);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.3);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(255, 255, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);

			rcTemp.left = rc.left + (rc.right / 1.3);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1.14);
			rcTemp.bottom = rc.bottom;


			hbrush = CreateSolidBrush(RGB(255, 255, 255));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		case 0:
			
			rcTemp.left = rc.left + (rc.right / 1.14);//ha badalnare rcTemp.left = rc.left+rc.right;
			rcTemp.top = rc.top;
			rcTemp.right = (rc.right / 1);
			rcTemp.bottom = rc.bottom;

			hbrush = CreateSolidBrush(RGB(0, 0, 0));
			SelectObject(hdc, hbrush);
			FillRect(hdc, &rcTemp, hbrush);
			break;
		}
		ReleaseDC(hwnd, hdc);
		EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


