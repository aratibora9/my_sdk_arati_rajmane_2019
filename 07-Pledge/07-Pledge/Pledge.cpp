//Headers
#include<Windows.h>

//global function declrations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	MessageBox(NULL, TEXT("Application is Started"), TEXT("MESSAGE"), MB_OK | MB_ICONINFORMATION);

	//code
	//initialization of WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;//callback funcion name is assigned here
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above  class
	RegisterClassEx(&wndClass);

	//create Window
	hwnd = CreateWindow(szAppName,
		TEXT("MY_APPLICATION"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);


	//message Loop


	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	TCHAR str[25];
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str_title[] =TEXT("\n\r***** PLEDGE *****\n\r");
	TCHAR str1[] = TEXT("\n\rIndia is My Country all Indians are my brothers and sisters.");
	TCHAR str2[] = TEXT("\n\r all Indians are my brothers and sisters.");
	TCHAR str3[] = TEXT("\n\rI love my country and I am proud of its rich and varied heritage.");
	TCHAR str4[]=TEXT("\n\rI shall always strive to be worthy of it.");
	TCHAR str5[] = TEXT("\n\rI shall give my parents,");
	TCHAR str6[]=TEXT("\n\rteachers and all elders respect and treat everyone with courtesy.");
	TCHAR str7[]=TEXT("\n\rTo my country and my people, I pledge my devotion.");
	TCHAR str8[]=TEXT("\n\rIn their well being and prosperity lies my happiness.");
	//code
	switch (iMsg)
	{
	case WM_CREATE:
		wsprintf(str, TEXT("WM_CREATE IS ARRIVED"));
		MessageBox(hwnd, str, TEXT("MESSAGE"), MB_OK | MB_ICONINFORMATION);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_PAINT:
		//Get changing rectangle(resize)
		GetClientRect(hwnd, &rc);
		//call specialist i.e. create the hdc
		hdc = BeginPaint(hwnd, &ps);
		
		//set background color
		SetBkColor(hdc, RGB(0, 0, 0));

		//set the text color
		//Orange color
		SetTextColor(hdc, RGB(255, 125, 0));
		//draw the actual text
		
		SetTextAlign(hdc, TA_CENTER);
		TextOut(hdc, 400, 10, str_title, wcslen(str_title));
		TextOut(hdc, 400, 30, str1, wcslen(str1));
		TextOut(hdc, 400, 50, str2, wcslen(str2));
		TextOut(hdc, 400, 70, str3, wcslen(str3));
		
		SetTextColor(hdc, RGB(255, 255, 255));
		TextOut(hdc, 400, 90, str4, wcslen(str4));
		TextOut(hdc, 400, 110, str5, wcslen(str5));
		TextOut(hdc, 400, 130, str6, wcslen(str6));

		SetTextColor(hdc, RGB(0, 255, 0));
		TextOut(hdc, 400, 150, str7, wcslen(str7));
		TextOut(hdc, 400, 170, str8, wcslen(str8));
		

		EndPaint(hwnd, &ps);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


