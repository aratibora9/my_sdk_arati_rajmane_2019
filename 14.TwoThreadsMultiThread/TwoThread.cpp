//Headers
#include<Windows.h>

//global function declrations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT WINAPI ThreadProcOne(LPVOID);
LRESULT WINAPI ThreadProcTwo(LPVOID);
static HWND ghWnd;
static HWND ghWnd1;
//Winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndClass;

	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	//code
	//initialization of WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;//callback funcion name is assigned here
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above  class
	RegisterClassEx(&wndClass);

	//create Window
	hwnd = CreateWindow(szAppName,
		TEXT("MY_APPLICATION"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghWnd = hwnd;
	ghWnd1 = hwnd;
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);


	//message Loop


	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HANDLE hThread1 = NULL;
	HANDLE hThread2 = NULL;

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		hThread1 = CreateThread(NULL,
			0,
			(LPTHREAD_START_ROUTINE)ThreadProcOne,
			(LPVOID)hwnd,
			0,
			NULL);
		hThread1 = CreateThread(NULL,
			0,
			(LPTHREAD_START_ROUTINE)ThreadProcTwo,
			(LPVOID)hwnd,
			0,
			NULL);		
		break;

	case WM_KEYDOWN:	
		switch (wParam)
		{
			
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
			}
		break;
	
	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("Left Mouse Button is Clicked"), TEXT("Message"), MB_OK);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

LRESULT WINAPI ThreadProcOne(LPVOID param)
{
	HDC hDc;
	long int  i;
	TCHAR str[255];
	hDc = GetDC((HWND)param);
	SetBkColor(hDc, RGB(0, 0, 0));
	SetTextColor(hDc, RGB(0, 255, 0));

	for (i = 0; i < 4294967295; i++)
	{
		wsprintf(str, TEXT("Thread One->Increasing Order:%ld"), i);
		TextOut(hDc, 5, 5, str, wcslen(str));
	}
	ReleaseDC(ghWnd, hDc);
	return 0;
}

LRESULT WINAPI ThreadProcTwo(LPVOID param)
{

	HDC hDc;
	double  i;
	TCHAR str1[255];
	hDc = GetDC((HWND)param);
	SetBkColor(hDc, RGB(0, 0, 0));
	SetTextColor(hDc, RGB(0, 255, 0));

	for (i = 4294967295; i>0; i--)
	{
		//4294967295
		
		wsprintf(str1, TEXT("Thread Two->Decreasing Order:%ld"), i);
		TextOut(hDc, 5,30, str1, wcslen(str1));
	}
	ReleaseDC(ghWnd1, hDc);
	return 0;
}