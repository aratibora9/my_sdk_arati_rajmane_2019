//Headers
#include<Windows.h>

//global function declrations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	MessageBox(NULL, TEXT("Application is Started"), TEXT("MESSAGE"), MB_OK | MB_ICONINFORMATION);

	//code
	//initialization of WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;//callback funcion name is assigned here
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above  class
	RegisterClassEx(&wndClass);

	//create Window
	hwnd = CreateWindow(szAppName,
		TEXT("MY_APPLICATION"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);


	//message Loop


	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	TCHAR str[25];
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str1[] = TEXT("Hello World !!!");
	//code
	switch (iMsg)
	{
	case WM_CREATE:
		wsprintf(str, TEXT("WM_CREATE IS ARRIVED"));
		MessageBox(hwnd, str, TEXT("MESSAGE"), MB_OK | MB_ICONINFORMATION);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_PAINT:
		//Get changing rectangle(resize)
		GetClientRect(hwnd, &rc);
		//call specialist i.e. create the hdc
		hdc = BeginPaint(hwnd, &ps);
		//set the text color
		SetTextColor(hdc, RGB(0, 255, 0));
		//set background color
		SetBkColor(hdc, RGB(0, 0, 0));
		//draw the actual text
		DrawText(hdc, str1, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		//sent back the actual specialist and stop painting
		EndPaint(hwnd, &ps);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


