//Writing Client of DLL using 
//Headers
#include<windows.h>
#include "Source.h"
//global function declrations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");


	//code
	//initialization of WNDCLASSEX
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;//callback funcion name is assigned here
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above  class
	RegisterClassEx(&wndClass);

	//create Window
	hwnd = CreateWindow(szAppName,
		TEXT("MY_APPLICATION"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);


	//message Loop


	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HINSTANCE hDll = NULL;
	typedef int(*MakeSquare)(int);
	MakeSquare pfn = NULL;
	int i, j;
	i = 25;
	TCHAR str[255];
	//code
	switch (iMsg)
	{
	case WM_CREATE:
		hDll = LoadLibrary(TEXT("10_DLL.dll"));
		//C:\SDKAssignments\12.DLLClinet\
		//here there should be error checking for 
		if (hDll == NULL)
		{
			MessageBox(hwnd, TEXT("Error in Loading DLL"), TEXT("Error hDll"), MB_OK);
		}
		pfn = (MakeSquare)GetProcAddress(hDll, "MakeSquare");
		if (pfn == NULL)
		{
			MessageBox(hwnd, TEXT("Error in Getting Address for pfn"), TEXT("Error pfn"), MB_OK);
		}
		j = pfn(i);
		wsprintf(str, TEXT("Square of %d is %d "), i, j);
		MessageBox(hwnd, str, TEXT("Output"), MB_OK);
		DestroyWindow(hwnd);
		FreeLibrary(hDll);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


